public class BookDetails {
    private int bookId;
    private int serviceCode;
    private int userId;
    private int itemId;

    public BookDetails(){

    }

    public BookDetails(int bookId, int serviceCode, int userId, int itemId){
        this.bookId = bookId;
        this.serviceCode = serviceCode;
        this.userId = userId;
        this.itemId = itemId;
    }

    public int getBookId() {
        return bookId;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public int getUserId() {
        return userId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
