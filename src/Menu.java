import java.time.LocalTime;

public class Menu {
    private int serviceCode;
    private String serviceName;
    private int price;
    private LocalTime estimatedTime;

    public Menu() {

    }

    public Menu(int serviceCode, String serviceName, int price, LocalTime estimatedTime){
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.price = price;
        this.estimatedTime = estimatedTime;
    }

    public int getServiceCode() {
        return serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public int getPrice() {
        return price;
    }

    public LocalTime getEstimatedTime() {
        return estimatedTime;
    }

}
