public class Account {
    private int accountNum;
    private String email;
    private String username;
    private String password;
    private int userID;

    public Account(){

    }

    public Account(int accountNum, String email, String username, String password, int userID ){
        this.accountNum = accountNum;
        this.email = email;
        this.username = username;
        this.password = password;
        this.userID = userID;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getUserID() {
        return userID;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
