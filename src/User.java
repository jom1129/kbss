public class User {
    private int userID;
    private String name;
    private String contNum;
    private String userType;

    public User() {

    }

    public User(int userID, String name, String contNum, String userType){
        this.userID = userID;
        this.name = name;
        this.contNum = contNum;
        this.userType = userType;
    }

    public int getUserID(){
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getContNum() {
        return contNum;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContNum(String contNum) {
        this.contNum = contNum;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
