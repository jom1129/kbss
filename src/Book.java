import java.math.BigInteger;

public class Book {
    private int bookID;
    private int userID;
    private int schedID;
    private String paymentMethod;
    private String credNum;
    private int isCancelled;
    private int amount;

    public Book() {

    }

    public Book(int bookID, int userID,int schedID, String paymentMethod, String credNum, int isCancelled, int amount) {
        this.bookID = bookID;
        this.userID = userID;
        this.schedID = schedID;
        this.paymentMethod = paymentMethod;
        this.credNum = credNum;
        this.isCancelled = isCancelled;
        this.amount = amount;
    }

    public int getBookID() {
        return bookID;
    }

    public int getUserID() {
        return userID;
    }

    public int getSchedID() {
        return schedID;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getCredNum() {
        return credNum;
    }

    public int getIsCancelled() {
        return isCancelled;
    }

    public int getAmount() {
        return amount;
    }

    public int setBookID(int bookID) {
        return bookID;
    }

    public int setUserID(int userID) {
        return userID;
    }

    public int setSchedID(int schedID) {
        return schedID;
    }

    public String setPaymentMethod(String paymentMethod) {
        return paymentMethod;
    }

    public int setCredNum(int credNum) {
        return credNum;
    }

    public int setIsCancelled(int isCancelled) {
        return isCancelled;
    }

    public int setAmount(int amount) {
        return amount;
    }
}
