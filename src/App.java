import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.time.*;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws SQLException {
        DataKBSS.setCon();
        boolean checkLogin = false;
        boolean goBack = true;
        while(goBack) {
            goBack = false;
            Scanner scanner = new Scanner(System.in);
            User user = new User();
            System.out.println("Welcome to Krazy Beauty Spa and Salon");
            System.out.println("Choose a number");
            System.out.println("-----------------------------------");
            System.out.println("1. Login");
            System.out.println("2. Create an account");
            System.out.println("3. Exit the app");
            System.out.println();
            int input = Integer.parseInt(scanner.nextLine());
            while (input > 3 || input < 1) {
                System.out.println("Invalid Number");
                input = Integer.parseInt(scanner.nextLine());
            }
            switch (input) {
                case 1:
                    user = login();
                    if (user.getUserType().equals("employee")) {
                        eMenu(user);
                    } else {
                        cMenu(user);
                    }
                    goBack = true;
                    System.out.println("Logged out");
                    break;


                case 2:
                    createAccount();
                    goBack = true;
                    break;
                case 3:
                    System.exit(0);
            }
        }
        DataKBSS.closeCon();
        //switch ()
    }

    public static User login(){
        Scanner scanner = new Scanner(System.in);
        boolean check = false;
        int userID = 0;
        User userL = new User();
        while (!check) {
            System.out.println("Enter your username...");
            String username = scanner.nextLine();
            System.out.println("Enter your password...");
            String password = scanner.nextLine();
            ArrayList<Account> accountArrayList = DataKBSS.getAccounts();
            for (Account account: accountArrayList){
                if ((account.getUsername()).equals(username)){
                    if ((account.getPassword()).equals(password)){
                        check = true;
                        userID = account.getUserID();
                        System.out.println("Login successful");
                        break;
                    }
                }
            }
            if(!check) {
                System.out.println("Invalid Credentials");
                System.out.println();
            }
        }


        ArrayList<User> userArrayList = DataKBSS.getUsers();
        for(User user: userArrayList){
            if(user.getUserID() == userID){
                userL = user;
            }
        }
        return userL;
    }



    public static void createAccount(){
        Scanner scanner = new Scanner(System.in);
        boolean check = false;
        int userID = 0;
        int accountID = 0;
        String jobDesc = "";
        System.out.println("Choose account type");
        System.out.println("-----------------------------------");
        System.out.println("1. Employee");
        System.out.println("2. Customer");
        System.out.println();

        int input = Integer.parseInt(scanner.nextLine());
        String userType = "";
        switch (input){
            case 1:
                userType = "employee";
                break;
            case 2:
                userType = "customer";
                break;
        }
        System.out.println("Enter your email");
        String email = scanner.nextLine();
        System.out.println("Enter your full name");
        String name = scanner.nextLine();
        System.out.println("Enter your contact number");
        String cN = scanner.nextLine();
        System.out.println("Enter your username");
        String username = scanner.nextLine();
        System.out.println("Enter your password");
        String password = scanner.nextLine();
        if(userType.equals("employee")) {
            System.out.println("Choose your job name");
            System.out.println("1. Barber");
            System.out.println("2. Masseur");
            System.out.println("3. Esthetician");
            int inputJD = Integer.parseInt(scanner.nextLine());
            switch (inputJD) {
                case 1:
                    jobDesc = "Barber";
                    break;
                case 2:
                    jobDesc = "Masseur";
                    break;
                case 3:
                    jobDesc = "Esthetician";
                    break;
            }
        }

        ArrayList<User> userArrayList = DataKBSS.getUsers();

        for (User user: userArrayList){
            if(userType.equals("employee")) {
                if(user.getUserType().equals("employee")){
                    if (userID < user.getUserID()){
                        userID = user.getUserID();
                    }
                }
            } else {
                if(user.getUserType().equals("customer")){
                    if (userID < user.getUserID()){
                        userID = user.getUserID();
                    }
                }
            }

        }
        userID++;

        User newUser = new User(userID, name, cN, userType);

        ArrayList<Account> accountArrayList = DataKBSS.getAccounts();
        for(Account account: accountArrayList){
            accountID = account.getAccountNum();
        }
        accountID++;

        Rooster rooster = null;
        if(newUser.getUserType().equals("employee")){
            rooster = new Rooster(newUser.getUserID(), jobDesc);
        }

        Account newAccount = new Account(accountID, email, username, password, userID);

        DataKBSS.createUser(newUser, newAccount, rooster);

        System.out.println("Account created Successfully");
        System.out.println();
    }

    /**
     * runs the employee-side features
     * @param user
     */
    public static void eMenu(User user){
        System.out.println();
        System.out.println("Hello! " + user.getName());
        employeeMenu(user);
    }

    public static void cMenu(User user){
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("Hello! " + user.getName());
        boolean goBack = true;
        while(goBack) {
            goBack = false;
            System.out.println();
            System.out.println("Choose a number");
            System.out.println("-----------------------------------");
            System.out.println("1. Book a schedule");
            System.out.println("2. Cancel a schedule");
            System.out.println("3. View menu");
            System.out.println("4. Logout");
            System.out.print(user.getName()+": ");
            int input = Integer.parseInt(scanner.nextLine());
            System.out.println();
            switch (input) {
                case 1:
                    bookSched(user);
                    goBack = true;
                    break;
                case 2:
                    cancelSched(user);
                    goBack = true;
                    break;
                case 3:
                    viewMenu();
                    goBack = true;
                    break;
                case 4:
                    return;
            }
        }
    }

    public static void bookSched(User user){
        Scanner scanner = new Scanner(System.in);
        //Chosen date
        boolean nSched = true;
        boolean dateCheck = false;
        LocalDate date = null;
        while (!dateCheck) {
            System.out.println("Enter your booking date (yy/mm/dd)");
            System.out.print(user.getName()+": ");
            String inputDate = scanner.nextLine();
            System.out.println();
            String[] arrayDate = inputDate.split("/");
            int year = Integer.parseInt(arrayDate[0])+2000;
            int month = Integer.parseInt(arrayDate[1]);
            int day = Integer.parseInt(arrayDate[2]);
            LocalDate t1 = LocalDate.of(year, month, day);
            LocalDate t2 = LocalDate.now();
            date = null;
            if (t1.compareTo(t2) > 0) {
                date = t1;
                dateCheck = true;
            } else {
                System.out.println("Entered booking date is past the present date");
            }
        }


        //Chosen time
        LocalTime time = LocalTime.of(0,0);
        boolean available = false;
        LocalDateTime dateTime = null;
        int schedId = 0;
        int input1 = 0;
        while (!available) {
            System.out.println("Select time");
            System.out.println("-----------------------------------");
            System.out.println("1. 10:00 AM");
            System.out.println("2. 12:00 PM");
            System.out.println("3. 02:00 PM");
            System.out.println("4. 04:00 PM");
            System.out.println("5. 06:00 PM");
            System.out.println();
            System.out.print(user.getName()+": ");
            input1 = Integer.parseInt(scanner.nextLine());
            System.out.println();
            available = true;
            switch (input1) {
                case 1:
                    time = LocalTime.of(10, 0, 0);
                    break;
                case 2:
                    time = LocalTime.of(12, 0, 0);
                    break;
                case 3:
                    time = LocalTime.of(14, 0, 0);
                    break;
                case 4:
                    time = LocalTime.of(16, 0, 0);
                    break;
                case 5:
                    time = LocalTime.of(18, 0, 0);
                    break;
            }

            ArrayList<Schedule> scheduleArrayList = DataKBSS.getAllSchedule();
            dateTime = LocalDateTime.of(date, time);
            for (Schedule schedule : scheduleArrayList) {
                if (schedule.getDateTime().isEqual(dateTime)) {
                    if (schedule.getIsVacant() == 1) {
                        schedId = schedule.getSchedID();
                        nSched=false;
                    } else {
                        System.out.println("This schedule is not available");
                        available = false;
                    }

                }
            }

            if(available) {
                if (schedId == 0) {
                    for (Schedule schedule : scheduleArrayList) {
                        if (schedId < schedule.getSchedID()) {
                            schedId = schedule.getSchedID();
                        }
                    }
                    schedId++;
                }
            }
            

        }

        Schedule schedule = new Schedule(schedId, dateTime, 0);

        

        // Chosen service/s
        ArrayList<User> appointedEmployees = getAppointedEmployee(schedId);
        ArrayList<User> unAppointedEmployees = new ArrayList<>();
        ArrayList<Rooster> roosterArrayList1 = DataKBSS.getRooster();
        ArrayList<User> userArrayList = DataKBSS.getUsers();
        ArrayList<Integer> availableChoices = new ArrayList<>();
        boolean isUnappointed;
        if (appointedEmployees.size() == 0){
            for (User user1: userArrayList){
                if (user1.getUserID() < 300){
                    unAppointedEmployees.add(user1);
                }
            }
        } else {
            for (User user2: userArrayList) {
                if(user2.getUserID() < 300){
                    isUnappointed = true;
                    for(User user1: appointedEmployees){
                        if (user1.getUserID() == user2.getUserID()) {
                            isUnappointed = false;
                            break;
                        }
                    }
                    if(isUnappointed){
                        unAppointedEmployees.add(user2);
                    }
                }
            }
        }





        System.out.println("How many services do you want to avail? (Max: 3)");
        System.out.print(user.getName()+": ");
        int c = Integer.parseInt(scanner.nextLine());
        System.out.println();
        if(c > 3){
            System.out.println("Invalid Input");
            System.out.print(user.getName()+": ");
            c = Integer.parseInt(scanner.nextLine());
            System.out.println();
        }
        ArrayList<Menu> menuArrayList = DataKBSS.getMenu();
        ArrayList<Menu> menuChoices = new ArrayList<>();
        String status = "";
        while (c>0){
            System.out.println("Choose the service/s that you want to avail (Choose 1 at a time)");
            System.out.println("-----------------------------------");
            int c1 = 1;
            System.out.printf("%-20s%-7s%-18s%s\n","Service" , "Price",  "Estimated Time" ,"Status");
            for(Menu menu: menuArrayList){
                status = "Unavailable in this schedule";
                System.out.printf("%-1d%-2s%-15s%-2s%-5s%-2s%-17s", c1 ,". ", menu.getServiceName() , " " , menu.getPrice() , " " , menu.getEstimatedTime());
                for(User user1: unAppointedEmployees){
                    if(menuChoices.contains(menu)){
                        status = "Chosen";
                    }
                    if(menu.getServiceName().equals("Haircut")){
                        for(Rooster rooster: roosterArrayList1){
                            if(user1.getUserID() == rooster.getUserId()){
                                if (rooster.getJobDesc().equals("Barber")){
                                    status = "Available";
                                }
                            }
                        }
                    }
                    if(menu.getServiceName().equals("Massage")){
                        for(Rooster rooster: roosterArrayList1){
                            if(user1.getUserID() == rooster.getUserId()){
                                if (rooster.getJobDesc().equals("Masseur")){
                                    status = "Available";
                                }
                            }
                        }
                    }
                    if(menu.getServiceName().equals("Facial Waxing")){
                        for(Rooster rooster: roosterArrayList1){
                            if(user1.getUserID() == rooster.getUserId()){
                                if (rooster.getJobDesc().equals("Esthetician")){
                                    status = "Available";
                                }
                            }
                        }
                    }
                }
                System.out.println(" "+ status);
                if(status.equals("Available")) {
                    availableChoices.add(c1);
                }
                c1++;

            }
            System.out.print(user.getName()+": ");
            int choice = Integer.parseInt(scanner.nextLine());
            System.out.println();
            if (!availableChoices.contains(choice)) {
                System.out.println("Chosen service is not available");
                System.out.print(user.getName()+": ");
                choice = Integer.parseInt(scanner.nextLine());
                System.out.println();
            }
            availableChoices.remove((Object) choice);

            menuChoices.add(menuArrayList.get(choice-1));
            c--;
            if(availableChoices.size() ==0){
                System.out.println("All services have been availed in this schedule");
                break;
            }
        }

        if(unAppointedEmployees.size() > menuChoices.size()){
            schedule.setVacant(1);
        } else {
            schedule.setVacant(0);
        }



        //Chosen payment method
        int totalAmount = 0;
        for(Menu menu: menuChoices){
            totalAmount = totalAmount + menu.getPrice();
        }
        String paymentMethod = "";
        System.out.println("Choose your payment method");
        System.out.println("-----------------------------------");
        System.out.println("1. Cash");
        System.out.println("2. Credit Card");
        System.out.print(user.getName()+": ");
        int choice = Integer.parseInt(scanner.nextLine());
        System.out.println();
        switch (choice){
            case 1: paymentMethod = "Cash"; break;
            case 2: paymentMethod = "Credit Card"; break;
        }

        //Card details

        String ccNo = "0" ;
        boolean cExists = false;
        CardDetails cardDetails = null;
        if(paymentMethod.equals("Credit Card")){
            System.out.println("Enter your credit card number");
            System.out.print(user.getName()+": ");
            ccNo = scanner.nextLine();
            System.out.println("Enter your credit card network");
            System.out.print(user.getName()+": ");
            String ccNW = scanner.nextLine();
            System.out.println("Enter your credit card's expiration date (mm/yy)");
            System.out.print(user.getName()+": ");
            String eD = scanner.nextLine();
            String stringCMonth = (eD.charAt(0) + String.valueOf(eD.charAt(1)));
            int cMonth = Integer.parseInt(stringCMonth);
            String stringCYear = (eD.charAt(3) + String.valueOf(eD.charAt(4)));
            int cYear = 2000 + Integer.parseInt(stringCYear);
            LocalDate expDate = LocalDate.of(cYear, cMonth, 1);
            System.out.println("Enter your CVV");
            System.out.print(user.getName()+": ");
            int cvv = Integer.parseInt(scanner.nextLine());
            System.out.println();
            cardDetails = new CardDetails(ccNo, user.getUserID(), ccNW, expDate, cvv);
            ArrayList<String> credNumArrayList = DataKBSS.getCardDetailsCredNum();
            for (String credNum: credNumArrayList){
                if(credNum.equals(ccNo)){
                    cExists = true;
                }
            }
        }

        //Book
        int bookId = 0;
        ArrayList<Book> bookArrayList = DataKBSS.getBooks();
        for(Book book: bookArrayList){
            if(bookId < book.getBookID()) {
                bookId = book.getBookID();
            }
        }
        bookId++;
        Book userBook = new Book(bookId, user.getUserID(), schedule.getSchedID(), paymentMethod, ccNo, 0, totalAmount);

        //Bookdetails
        ArrayList<Rooster> roosterArrayList = DataKBSS.getRooster();
        ArrayList<BookDetails> bookDetailsArrayList = new ArrayList<>();

        ArrayList<User> employees = DataKBSS.getUsers();
        int empID=0;
        for(Menu menu: menuChoices){
            if (menu.getServiceName().equals("Haircut")){
                for(Rooster rooster: roosterArrayList){
                    if (rooster.getJobDesc().equals("Barber")){
                        empID = rooster.getUserId();
                    }
                }
            } else if (menu.getServiceName().equals("Massage")){
                for(Rooster rooster: roosterArrayList){
                    if (rooster.getJobDesc().equals("Masseur")){
                        empID = rooster.getUserId();
                    }
                }
            }
            else if (menu.getServiceName().equals("Facial Waxing")){
                for(Rooster rooster: roosterArrayList){
                    if (rooster.getJobDesc().equals("Esthetician")){
                        empID = rooster.getUserId();
                    }
                }
            }
            BookDetails bookDetailsTemp = new BookDetails(bookId, menu.getServiceCode(), empID, 0);
            bookDetailsArrayList.add(bookDetailsTemp);
        }

        if(nSched){
            DataKBSS.bookNewSched(schedule, cardDetails, userBook, bookDetailsArrayList, cExists);
        } else {
            schedule.setVacant(0);
            DataKBSS.updateSched(schedule);
            DataKBSS.bookSched(cardDetails, userBook, bookDetailsArrayList, cExists);
        }
        System.out.println("Booking Successful!");


    }


    public static void cancelSched(User user) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Schedule> scheduleArrayList = DataKBSS.getAllSchedule();
        ArrayList<Schedule> schedulesOfUser = new ArrayList<>();
        ArrayList<Book> bookArrayList = DataKBSS.getBooks();
        ArrayList<Book> booksOfUser = new ArrayList<>();
        for(Book book: bookArrayList){
            if(user.getUserID() == book.getUserID()){
                booksOfUser.add(book);
            }
        }
        int c = 1;

        for(Book book : booksOfUser){
            for(Schedule schedule: scheduleArrayList){
                if(book.getSchedID() == schedule.getSchedID()){
                    if(book.getIsCancelled()==0) {
                        schedulesOfUser.add(schedule);
                    }
                }
            }
        }
        System.out.println("Select a booked date that you want to cancel");
        for(Schedule schedule : schedulesOfUser){

            System.out.println(c+". "+ schedule.getDateTime());
            c++;
        }

        if(schedulesOfUser.size()!=0){
            System.out.print(user.getName()+": ");
            int input = Integer.parseInt(scanner.nextLine());
            Schedule cancelledSchedule = schedulesOfUser.get(input-1);

            Book cancelledBook = new Book();
            for (Book book: bookArrayList){
                if(book.getSchedID() == cancelledSchedule.getSchedID()){
                    if(book.getUserID()== user.getUserID()){
                        cancelledBook = book;
                    }
                }
            }

            DataKBSS.cancelSched(cancelledSchedule, cancelledBook);
            System.out.println("Schedule Canceled Successfully");

        } else {
            System.out.println("There are no booked dates");
        }

    }

    public static void viewMenu(){
        ArrayList<Menu> menuArrayList = DataKBSS.getMenu();
        Scanner scanner = new Scanner(System.in);
        int c1 =1;
        for(Menu menu: menuArrayList){
            System.out.println(c1 +". "+ menu.getServiceName() + " " + menu.getPrice() + " " + menu.getEstimatedTime());
            c1++;
        }
        System.out.println("Press Enter to go back");
        System.out.println("...");
        String input = scanner.nextLine();
    }

    /**
     * The menu for employee that prompts the user to show the schedule or exit.
     */
    public static void employeeMenu(User user) {
        Scanner input = new Scanner(System.in);
        System.out.println("Choose an option");
        System.out.println("-----------------------------------");
        System.out.println("1. Show Schedules");
        System.out.println("2. Exit");
        System.out.println();
        System.out.print(user.getName()+": ");
        int choice = Integer.parseInt(input.nextLine());
        if (choice == 1) {
            displaySchedule(user);
        } else if (choice == 2) {
            System.exit(0);
        }
    }

    /**
     * Display all schedule and numbers them as well as
     * prompts the user to choose a feature. Edit schedules,
     * Update customer, or go back to the previous menu.
     */
    public static void displaySchedule(User user) {
        Scanner input = new Scanner(System.in);
        ArrayList<Schedule> scheduleList = DataKBSS.getAllSchedule();
        int scheduleNum = 0;
        System.out.println();
        System.out.println("Schedules: ");
        for (Schedule schedules : scheduleList) {
            scheduleNum++;
            System.out.println(scheduleNum + ". " + schedules.getDateTime());
        }
        System.out.println("-----------------------------------");
        System.out.println();
        System.out.println("1. Edit schedules");
        System.out.println("2. Update costumer's service status");
        System.out.println("3. Go back");
        System.out.println();
        boolean isAnswerInvalid = false;
        do {
            System.out.print(user.getName()+": ");
            int choice = Integer.parseInt(input.nextLine());
            if (choice == 1) {
                editSchedule(user);
            } else if (choice == 2) {
                updateCostumerStatus(user);
            } else if (choice == 3){
                employeeMenu(user);
            } else {
                isAnswerInvalid = true;
            }
        } while (isAnswerInvalid);
    }

    /**
     * Method that can mark a schedule as busy or vacant
     */
    public static void editSchedule(User user) {
        Scanner input = new Scanner(System.in);
        ArrayList<Schedule> listOfSchedule = DataKBSS.getAllSchedule();
        System.out.println("Select the number of schedule to be edited");
        int choice = Integer.parseInt(input.nextLine());
        Schedule selectedSchedule = listOfSchedule.get(choice - 1);
        System.out.println("-----------------------------------");
        System.out.println(selectedSchedule.getDateTime());
        System.out.println();
        System.out.println("1. Mark as vacant");
        System.out.println("2. Mark as busy");
        System.out.println("3. Go back");
        System.out.print(user.getName()+": ");
        int editChoice = Integer.parseInt(input.nextLine());
        switch (editChoice) {
            case 1 -> {
                DataKBSS.markAsVacant(selectedSchedule);
                displaySchedule(user);
            }
            case 2 -> {
                DataKBSS.markAsBusy(selectedSchedule);
                displaySchedule(user);
            }
            case 3 -> employeeMenu(user);
            default -> displaySchedule(user);
        }
    }

    /**
     * Figuratively sends the costumer an update regarding their schedule.
     */
    public static void updateCostumerStatus(User user) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter customer name: ");
        System.out.print(user.getName()+": ");
        String costumerName = input.nextLine();
        System.out.println("Type message below");
        input.nextLine();
        System.out.println();
        System.out.println("Sending status update to Ma'am/Sir" + costumerName);
        employeeMenu(user);
    }

    public static ArrayList<User> getAppointedEmployee(int schedID){
        ArrayList<User> employeeList = new ArrayList<>();
        ArrayList<Integer> employeeIDList = new ArrayList<>();
        ArrayList<Integer> bookIDArrayList = new ArrayList<>();
        for (Book b: DataKBSS.getBooks())
            if(schedID == b.getSchedID())
                bookIDArrayList.add(b.getBookID());

        for(Integer i : bookIDArrayList){
            for(BookDetails bd: DataKBSS.getBookDetails()){
                if(i == bd.getBookId()){
                    for(User u : DataKBSS.getUsers()) {
                        if (u.getUserID() == bd.getUserId()) {
                            if(!employeeIDList.contains(u.getUserID()))
                                employeeIDList.add(u.getUserID());
                        }
                    }
                }
            }
        }

        for(User u : DataKBSS.getUsers()){
            for(Integer i : employeeIDList){
                if(u.getUserID() == i){
                    employeeList.add(u);
                }
            }
        }
        return employeeList;
    }
}

