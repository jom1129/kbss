import java.time.LocalDateTime;

public class Schedule {
    private int schedID;
    private LocalDateTime dateTime;
    private int isVacant;

    public Schedule() {
    }

    public Schedule (int schedID, LocalDateTime dateTime, int isVacant){
        this.schedID = schedID;
        this.dateTime = dateTime;
        this.isVacant = isVacant;
    }

    public int getSchedID() {
        return schedID;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public int getIsVacant() {
        return isVacant;
    }

    public void setSchedID(int schedID) {
        this.schedID = schedID;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setVacant(int vacant) {
        isVacant = vacant;
    }
}
