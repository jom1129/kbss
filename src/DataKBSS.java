import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class DataKBSS {
    private  static  Connection con;

    private DataKBSS(){

    }

    public static void setCon() {
        try {
            String conStr = "jdbc:mysql://localhost:3306/krazy_beauty_spa_and_salon?user=root&password=";
            con = DriverManager.getConnection(conStr);
        } catch (Exception e){

        }
    }

    /*
    Inserts a new user and account in the database after the user has created its account
     */
    public static void createUser(User user, Account account, Rooster rooster){
        PreparedStatement userStatement;
        PreparedStatement accountStatement;
        PreparedStatement roosterStatement;
        String userStr = "INSERT INTO user(user_id, name, contact_number, user_type)" +
                "VALUES(?,?,?,?)";

        String accountStr = "INSERT INTO accounts(account_no, email, username, password, user_id)" +
                "VALUES(?,?,?,?,?)";

        String roosterStr = "INSERT INTO rooster(user_id, job_description)" +
                "VALUES(?,?)";

        try {
            userStatement = con.prepareStatement(userStr);
            userStatement.setInt(1, user.getUserID());
            userStatement.setString(2, user.getName());
            userStatement.setString(3, user.getContNum());
            userStatement.setString(4, user.getUserType());
            userStatement.execute();

            accountStatement = con.prepareStatement(accountStr);
            accountStatement.setInt(1, account.getAccountNum());
            accountStatement.setString(2, account.getEmail());
            accountStatement.setString(3, account.getUsername());
            accountStatement.setString(4, account.getPassword());
            accountStatement.setInt(5, user.getUserID());
            accountStatement.execute();

            if(rooster != null){
                roosterStatement = con.prepareStatement(roosterStr);
                roosterStatement.setInt(1, rooster.getUserId());
                roosterStatement.setString(2, rooster.getJobDesc());
                roosterStatement.execute();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /*
    Inserts a new schedule, card details(if not null), book details and book in the database
    // if card number is 0 set it to null
     */
    public static void bookNewSched(Schedule schedule,  CardDetails cardDetails, Book book, ArrayList<BookDetails> bookDetailsArrayList, boolean cExists){
        PreparedStatement bookStatement;
        PreparedStatement schedStatement;
        PreparedStatement bookDetailsStatement;
        String bookStr = "INSERT INTO book(book_id, user_id, sched_id, payment_method, credit_card_number, is_canceled, amount)" +
                "VALUES(?,?,?,?,?,?,?)";

        String schedStr = "INSERT INTO schedule(sched_id, `date&time`, is_vacant)" +
                "VALUES(?,?,?)";


        String bookDetailsStr = "INSERT INTO book_details(book_id, service_code, user_id, item_id)" +
                "VALUES(?,?,?,?)";

        try {
            bookStatement = con.prepareStatement(bookStr);
            bookStatement.setInt(1, book.getBookID());
            bookStatement.setInt(2, book.getUserID());
            bookStatement.setInt(3, book.getSchedID());
            bookStatement.setString(4, book.getPaymentMethod());
            if(book.getCredNum().equals("0")){
                bookStatement.setNull(5, java.sql.Types.BIGINT);
            } else{
                bookStatement.setObject(5, book.getCredNum(), java.sql.Types.BIGINT);
            }
            bookStatement.setInt(6, book.getIsCancelled());
            bookStatement.setInt(7, book.getAmount());
            bookStatement.execute();

            schedStatement = con.prepareStatement(schedStr);
            schedStatement.setInt(1, schedule.getSchedID());
            Timestamp timestamp = Timestamp.valueOf(schedule.getDateTime());
            String str = String.valueOf(timestamp);
            schedStatement.setTimestamp(2, timestamp);
            schedStatement.setInt(3, schedule.getIsVacant());
            schedStatement.execute();

            if(!cExists) {
                if (cardDetails != null) {
                    DataKBSS.inputCardDetails(cardDetails);
                }
            }

            bookDetailsStatement = con.prepareStatement(bookDetailsStr);
            for(BookDetails bookDetails: bookDetailsArrayList){
                bookDetailsStatement.setInt(1, bookDetails.getBookId());
                bookDetailsStatement.setInt(2, bookDetails.getServiceCode());
                bookDetailsStatement.setInt(3, bookDetails.getUserId());
                bookDetailsStatement.setInt(4, bookDetails.getItemId());
                bookDetailsStatement.execute();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void bookSched(CardDetails cardDetails, Book book, ArrayList<BookDetails> bookDetailsArrayList, boolean cExists){
        PreparedStatement bookStatement;
        PreparedStatement bookDetailsStatement;
        String bookStr = "INSERT INTO book(book_id, user_id, sched_id, payment_method, credit_card_number, is_canceled, amount)" +
                "VALUES(?,?,?,?,?,?,?)";

        String bookDetailsStr = "INSERT INTO book_details(book_id, service_code, user_id, item_id)" +
                "VALUES(?,?,?,?)";

        try {
            bookStatement = con.prepareStatement(bookStr);
            bookStatement.setInt(1, book.getBookID());
            bookStatement.setInt(2, book.getUserID());
            bookStatement.setInt(3, book.getSchedID());
            bookStatement.setString(4, book.getPaymentMethod());
            if(book.getCredNum().equals("0")){
                bookStatement.setNull(5, java.sql.Types.BIGINT);
            } else{
                bookStatement.setObject(5, book.getCredNum(), java.sql.Types.BIGINT);
            }
            bookStatement.setInt(6, book.getIsCancelled());
            bookStatement.setInt(7, book.getAmount());
            bookStatement.execute();

            if(!cExists) {
                if (cardDetails != null) {
                    DataKBSS.inputCardDetails(cardDetails);
                }
            }

            bookDetailsStatement = con.prepareStatement(bookDetailsStr);
            for(BookDetails bookDetails: bookDetailsArrayList){
                bookDetailsStatement.setInt(1, bookDetails.getBookId());
                bookDetailsStatement.setInt(2, bookDetails.getServiceCode());
                bookDetailsStatement.setInt(3, bookDetails.getUserId());
                bookDetailsStatement.setInt(4, bookDetails.getItemId());
                bookDetailsStatement.execute();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /*
    Sets the schedule as vacant and the book as cancelled
     */
    public static void cancelSched(Schedule schedule, Book book){
        PreparedStatement bookStatement;
        PreparedStatement scheduleStatement;
        String bookst = "UPDATE book SET is_canceled = 1 WHERE book_id = ?";
        String schedst= "UPDATE schedule SET is_vacant = 1 WHERE sched_id = ?";
        try{
            bookStatement = con.prepareStatement(bookst);
            bookStatement.setInt(1, book.getBookID());
            bookStatement.execute();

            scheduleStatement = con.prepareStatement(schedst);
            scheduleStatement.setInt(1, schedule.getSchedID());
            scheduleStatement.execute();
        } catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }

    public static void markAsVacant(Schedule schedule) {
        PreparedStatement setVacantPs;
        String vacantStr = "UPDATE schedule SET is_vacant = 1 WHERE sched_id = ?";
        try {
            setVacantPs = con.prepareStatement(vacantStr);
            setVacantPs.setInt(1, schedule.getSchedID());
            setVacantPs.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void markAsBusy(Schedule schedule) {
        PreparedStatement setBusyPs;
        String busyStr = "UPDATE schedule SET is_vacant = 0 WHERE sched_id = ?";
        try {
            setBusyPs = con.prepareStatement(busyStr);
            setBusyPs.setInt(1, schedule.getSchedID());
            setBusyPs.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateSched(Schedule schedule){
        PreparedStatement schedPs;
        String schedStr = "UPDATE schedule SET is_vacant = ? WHERE sched_id = ?";
        try {
            schedPs = con.prepareStatement(schedStr);
            schedPs.setInt(1, schedule.getIsVacant());
            schedPs.setInt(2, schedule.getSchedID());
            schedPs.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /*
    Inserts a new Card Details in the database
     */
    public static void inputCardDetails(CardDetails cardDetails) {
        PreparedStatement ps;
        String st = "INSERT INTO card_details(credit_card_number, user_id, credit_card_network, expiration_date, cvv)" +
                "VALUES(?,?,?,?,?)";
        try {
            ps = con.prepareStatement(st);
            ps.setObject(1, cardDetails.getCredNum(), java.sql.Types.BIGINT);
            ps.setInt(2, cardDetails.getUserID());
            ps.setString(3, cardDetails.getCcNetwork());
            ps.setDate(4, java.sql.Date.valueOf(cardDetails.getExpDate()));
            ps.setInt(5, cardDetails.getCVV());
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /*
    Returns a list of menu and their prices
     */
    public static ArrayList<Menu> getMenu() {
        ArrayList<Menu> menuArrayList = new ArrayList<>();
        try {
            String command = "SELECT * FROM Menu;";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = state.executeQuery(command);
            resultSet.beforeFirst();
            while (resultSet.next()) {
                Menu menu = new Menu(Integer.parseInt(resultSet.getString(1)), resultSet.getString(2), Integer.parseInt(resultSet.getString(3)), LocalTime.parse(resultSet.getString(4)));
                menuArrayList.add(menu);
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return menuArrayList;
    }

    /*
    Returns list of accounts
     */
    public static ArrayList<Account> getAccounts() {
        ArrayList<Account> accountArrayList = new ArrayList<>();
        try {
            String command = "SELECT * FROM accounts;";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                Account account = new Account(Integer.parseInt(resultSet.getString(1)), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), Integer.parseInt(resultSet.getString(5)));
                accountArrayList.add(account);
            }
            resultSet.close();
        }catch(Exception e){
           e.printStackTrace();
       }
        return accountArrayList;
    }

    /*
    Returns list of users
     */
    public static ArrayList<User> getUsers() {
        ArrayList<User> userArrayList = new ArrayList<>();
        try {
            String command = "SELECT * FROM user";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                User newUser = new User(Integer.parseInt(resultSet.getString(1)), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4));
                userArrayList.add(newUser);
            }
            resultSet.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return userArrayList;
    }

    /*
    Returns list of book
     */
    public static ArrayList<Book> getBooks() {
        ArrayList<Book> bookArrayList = new ArrayList<>();
        try {
            String command = "SELECT * FROM book;";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                String ccNo = "0";
                String str = resultSet.getString(5);
                if(str!=null){
                    ccNo = resultSet.getString(5);
                }
                Book newBook = new Book(Integer.parseInt(resultSet.getString(1)), Integer.parseInt(resultSet.getString(2)), Integer.parseInt(resultSet.getString(3)), resultSet.getString(4),
                        ccNo, Integer.parseInt(resultSet.getString(6)), Integer.parseInt(resultSet.getString(7)));
                bookArrayList.add(newBook);
            }
            resultSet.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return bookArrayList;
    }

    /*
     Returns the list of all schedule
     */
    public static ArrayList<Schedule> getAllSchedule() {
        ArrayList<Schedule> scheduleArrayList = new ArrayList<>();
        try {
            String command = "SELECT * FROM schedule";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();

            while (resultSet.next()) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String str = resultSet.getString(2);
                LocalDateTime dateTime = LocalDateTime.parse(resultSet.getString(2), formatter);
                Schedule newSchedule = new Schedule(Integer.parseInt(resultSet.getString(1)), LocalDateTime.parse(resultSet.getString(2), formatter), Integer.parseInt(resultSet.getString(3)));
                scheduleArrayList.add(newSchedule);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return scheduleArrayList;

    }

    public static ArrayList<Rooster> getRooster() {
        ArrayList<Rooster> roosterArrayList = new ArrayList<>();
        try{
            String command = "SELECT * FROM rooster";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                Rooster rooster = new Rooster(Integer.parseInt(resultSet.getString(1)), resultSet.getString(2));
                roosterArrayList.add(rooster);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return roosterArrayList;
    }

    public static ArrayList<BookDetails> getBookDetails(){
        ArrayList<BookDetails> bookDetailsArrayList = new ArrayList<>();
        try{
            String command = "SELECT * FROM book_details";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                //(int bookId, int serviceCode, int userId, int itemId
                BookDetails bookDetails = new BookDetails(Integer.parseInt(resultSet.getString(1)),
                        Integer.parseInt(resultSet.getString(2)),
                        Integer.parseInt(resultSet.getString(3)),
                        0);
                bookDetailsArrayList.add(bookDetails);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return bookDetailsArrayList;
    }

    public static ArrayList<String> getCardDetailsCredNum(){
        ArrayList<String> credNumArrayList = new ArrayList<>();
        try{
            String command = "SELECT credit_card_number FROM card_details";
            Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE );
            ResultSet resultSet = state.executeQuery(command);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                String credNum = resultSet.getString(1);
                credNumArrayList.add(credNum);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return credNumArrayList;
    }

    public static void closeCon() throws SQLException {
        if(con != null){
            con.close();
        }
    }
}
