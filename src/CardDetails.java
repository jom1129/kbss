import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;

public class CardDetails {
    private String credNum;
    private int userID;
    private String ccNetwork;
    private LocalDate expDate;
    private int CVV;

    public CardDetails() {

    }

    public CardDetails(String credNum, int userID, String ccNetwork, LocalDate expDate, int CVV){
        this.credNum = credNum;
        this.userID = userID;
        this.ccNetwork = ccNetwork;
        this.expDate = expDate;
        this.CVV = CVV;
    }

    public String getCredNum() {
        return credNum;
    }

    public int getUserID() {
        return userID;
    }

    public String getCcNetwork() {
        return ccNetwork;
    }

    public LocalDate getExpDate() {
        return expDate;
    }

    public int getCVV() {
        return CVV;
    }

    public void setCredNum(String credNum) {
        this.credNum = credNum;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setCcNetwork(String ccNetwork) {
        this.ccNetwork = ccNetwork;
    }

    public void setExpDate(LocalDate expDate) {
        this.expDate = expDate;
    }

    public void setCVV(int CVV) {
        this.CVV = CVV;
    }
}
