public class Rooster {
    private int userId;
    private String jobDesc;

    public Rooster() {

    }

    public Rooster(int userId, String jobDesc){
        this.userId = userId;
        this.jobDesc = jobDesc;

    }

    public int getUserId() {
        return userId;
    }

    public String getJobDesc() {
        return jobDesc;
    }
}
